$(function(){

//AJOUTER UN COMMENTAIRE A L'ENVOI DU FORMULAIRE

$('#form_commentaires').submit(function(e){
  e.preventDefault();

  $.ajax({
    url: 'commentaire/add',
    data: { pseudo: $('#pseudo').val(),
            commentaire: $('#commentaire').val() },
    method: 'post'
  })
  .done(function(reponsePHP){
    $('#listeDesPosts').prepend(reponsePHP)
                       .find('li:first')
                       .hide()
                       .slideDown(function(){
                         $('#pseudo').val('');
                         $('#commentaire').val('');
                         });
  })
  .fail(function(){
    alert('Problème durant la transaction !');
  });
});

//SUPPRIMER UN COMMENTAIRE

$('#listeDesPosts').on('click', '.delete', function(e){
   e.preventDefault();
if (confirm('Êtes-vous sûr(e) de vouloir supprimer ce commentaire ?')){
   $.ajax({
    url: 'commentaire/delete/'+ $(this).closest('li').attr('data-id'),
    context: this
  })
  .done(function(reponsePHP){
    $(this).closest('li').slideUp(function(){
                            $(this).remove()
                            });
  })
  .fail(function(){
    alert('Problème durant la transaction !');
  });
}
});


//EDITER UN COMMENTAIRE : BOUTON EDITER

$('#listeDesPosts').on('click', '.edit', function(e){
   e.preventDefault();
   $(this).toggleClass('edit validate')
          .text('Valider la modification');
   let commentaire = $(this).closest('li').find('.text');
   let contenu = commentaire.text();
   commentaire.html('<input type="text" />')
              .find('input')
              .val(contenu);
   //let commentaire = $(this).closest('li').find('.text');
   //commentaire.html(`<input type="text" value="${commentaire.text()}">`);


  });

//EDITER UN COMMENTAIRE : BOUTON VALIDER

$('#listeDesPosts').on('click', '.validate', function(e){
   e.preventDefault();
   let commentaire = $(this).closest('li').find('.text input').val();
   $.ajax({
    url: 'commentaire/edit/' + $(this).closest('li').attr('data-id'),
    data: {
      commentaire: commentaire
    },
    method: 'post',
    context: this
  })
  .done(function(reponsePHP){
    $(this).closest('li').find('.text').html(commentaire);
    $(this).toggleClass('validate edit')
           .text('Editer le texte');
  })
  .fail(function(){
    alert('Problème durant la transaction !');
  });
});


$('#listeDesPosts').on('keyup', '.text input', function(e){
   e.preventDefault();
   if (e.keyCode === 13){
   let commentaire = $(this).val();
   $.ajax({
    url: 'commentaire/edit/' + $(this).closest('li').attr('data-id'),
    data: {
      commentaire: commentaire
    },
    method: 'post',
    context: this
  })
  .done(function(reponsePHP){
    $(this).closest('li').find('.validate').toggleClass('validate edit')
                                           .text('Editer le texte');
    $(this).closest('.text').html(commentaire);
  })
  .fail(function(){
    alert('Problème durant la transaction !');
  });
  }
});




//FIN WINDOW.ONLOAD
});
