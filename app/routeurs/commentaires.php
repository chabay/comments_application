<?php
/*
  ./app/routeurs/users.php
  Routes des users
  Il existe un $_GET['users']
*/
use \App\Controleurs\CommentairesControleur;

switch ($_GET['commentaire']) {

  /* AJOUTER UN COMMENTAIRE
  PATTERN : index.php?commentaire=add
  CTRL :  commentairesControleur
  ACTION : add */

  case 'add':
  include '../app/controleurs/commentairesControleur.php';
  CommentairesControleur\addAction($connexion, ['pseudo'=> $_POST['pseudo'], 'commentaire' => $_POST['commentaire']]);
  break;

  /* SUPPRIMER UN COMMENTAIRE
  PATTERN : index.php?commentaire=delete
  CTRL : commentairesControleur
  ACTION : delete */

  case 'delete':
  include '../app/controleurs/commentairesControleur.php';
  CommentairesControleur\deleteAction($connexion, $_GET['id']);
  break;


  /* EDITER UN COMMENTAIRE
  PATTERN : index.php?commentaire=edit
  CTRL : commentairesControleur
  ACTION : edit */

  case 'edit':
  include '../app/controleurs/commentairesControleur.php';
  CommentairesControleur\editAction($connexion, ['id'=> $_GET['id'], 'commentaire' => $_POST['commentaire']]);
  break;

}
