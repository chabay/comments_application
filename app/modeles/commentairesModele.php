<?php
/*
    ./app/modeles/commentairesModele.php
 */
namespace App\Modeles\CommentairesModele;

function findAll(\PDO $connexion) {
  $sql = 'SELECT *
          FROM commentaires
          ORDER BY created_at DESC;';
  $rs = $connexion->query($sql);

  return $rs->fetchAll(\PDO::FETCH_ASSOC);
}

function addOne(\PDO $connexion, array $data) {
  $sql = 'INSERT INTO commentaires
          SET pseudo = :pseudo,
              texte = :texte,
              created_at = NOW();';

  $rs = $connexion->prepare($sql);
  $rs->bindValue(':pseudo', $data['pseudo'], \PDO::PARAM_STR);
  $rs->bindValue(':texte', $data['commentaire'], \PDO::PARAM_STR);
  $rs->execute();

  return $connexion->lastInsertId();

}


function deleteOneById(\PDO $connexion, int $id) :bool {
  $sql = 'DELETE FROM commentaires
          WHERE id = :id;';

  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  return $rs->execute();


}


function editOneById(\PDO $connexion, array $data) :bool {
  $sql = 'UPDATE commentaires
          SET texte = :texte
          WHERE id = :id;';

  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $data['id'], \PDO::PARAM_INT);
  $rs->bindValue(':texte', $data['commentaire'], \PDO::PARAM_STR);
  return $rs->execute();


}
