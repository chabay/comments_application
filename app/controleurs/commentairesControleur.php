<?php
/*
    ./app/controleurs/commentairesControleur.php
 */
namespace App\Controleurs\CommentairesControleur;
use \App\Modeles\CommentairesModele AS Commentaire;

function indexAction(\PDO $connexion) {
  //Je vais chercher la liste des commentaires
  include '../app/modeles/commentairesModele.php';
  $commentaires = Commentaire\findAll($connexion);
  //Je charge la vue index dans $content1
  GLOBAL $content1, $title;
  $title = COMMENTAIRES_INDEX_TITLE;
  ob_start();
  include '../app/vues/commentaires/index.php';
  $content1 = ob_get_clean();
}

function addAction(\PDO $connexion, array $data) {

  include '../app/modeles/commentairesModele.php';
  $commentaireID = Commentaire\addOne($connexion, $data);

  include '../app/vues/commentaires/add.php';

}



function deleteAction(\PDO $connexion, int $id) {

  include '../app/modeles/commentairesModele.php';
  echo Commentaire\deleteOneById($connexion, $id);

}




function editAction(\PDO $connexion, array $data) {
  //Je demande au modèle de modifier le commentaire dans la db
  include '../app/modeles/commentairesModele.php';
  echo Commentaire\editOneById($connexion, $data);


}
