<?php
/*
  ./app/routeur.php
 */

 /*
 ROUTE PAR DEFAUT
 PATTERN : commentaire/add
 CTRL : commentairesControleur
 Action : add
  */


 if (isset($_GET['commentaire'])):
   include '../app/routeurs/commentaires.php';
   

 /*
 ROUTE PAR DEFAUT
 PATTERN : /
 CTRL : commentairesControleur
 Action : index
  */

 else:
   include '../app/controleurs/commentairesControleur.php';
   \App\Controleurs\CommentairesControleur\indexAction($connexion);
 endif;
