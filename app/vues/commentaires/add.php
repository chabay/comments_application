<?php
/*
    ./app/vues/commentaires/add.php
    Variables disponibles :
    - $commentaireID, $data['pseudo'], $data['commentaire']
*/
?>


        <li data-id="<?php echo $commentaireID; ?>" class="collection-item avatar post">
            <i class="material-icons circle green">insert_chart</i>
            <div class="title"><?php echo $data['pseudo']; ?></div>
            <div class="text truncate"><?php echo $data['commentaire']; ?></div>
            <div><a href="#" class="edit">Editer le texte</a> | <a href="#" class="delete">Supprimer la publication</a></div>
        </li>
